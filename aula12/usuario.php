<?php
class Usuario {
    private $id;
    private $nome;
    private $email;
    private $senha;

    private $objDb;

    public function __construct(){
        $this->objDb = new mysqli('localhost','root','','aula_php',3307);
           // var_dump($this->objDb);
        }


    public function setId(int $id){
        $this->id = $id;
    }

    public function setNome(string $nome){
        $this->nome = $nome;
    }

    public function setEmail(string $email){
        $this->email = $email;
    }

    public function setSenha(string $senha){
        return $this->senha = password_hash($senha, PASSWORD_DEFAULT);
        
    }

    public function getId() : int{
        return $this->$id;
    }

    public function getNome() : string{
        return $this->$nome;
    }

    public function getEmail() : string{
        return $this->$email;
    }

    public function getSenha() : string{
       return $this->$senha = $senha;
    }

    public function getUser (int $id): int{
       return array ($this->id, $this->nome, $this->email);
    }


    public function saveUsuario(){
        $objStmt = $this->objDb->prepare('REPLACE INTO tb_usuario(id, nome, email, senha) VALUES (?,?,?,?)');

        $objStmt-> bind_param('isss', $this->id, $this->nome, $this->email, $this->senha);

        if($objStmt->execute()){
            return true;
        }else{
            return false;   
        }
    }

    public function deleteUsuario(){
        $objStmt = $this->objDb->prepare('DELETE FROM tb_usuario WHERE id = ?');

        $objStmt-> bind_param('i', $this->id);

        if($objStmt->execute()){
            return true;
        }else{
            return false;   
        }
    }

    public function findUsuario(){

        $query = "SELECT id, nome, email, senha FROM tb_usuario";
        if ($result = $this->objDb->query($query)) {
            while ($row = $result->fetch_row()) {
                printf("%s (%s,%s)\n", $row[0], $row[1], $row[2]);
            }
            /* free result set */
            $result->close();
        }
        
    }
    

    public function _destruct(){
        unset($this->objDB);
    }
}
