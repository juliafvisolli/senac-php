<?php

//Mostra detalhe do ambiente
//phpinfo();


    //echo '<pre>';
        //var_dump ($_SERVER);

    //echo "Seu IP é" . $_SERVER ['REMOTE_ADDR'];

    //echo "Seu IP é :  {$_SERVER ['REMOTE_ADDR']}";
    
//Vetor com mais de uma dimensão

//$alunos[0]['nome'] = 'Fulano';
//$alunos[0]['bitbucket'] = 'https://bitbucket...';
//$alunos[1]['nome'] = 'Fulano1';
//$alunos[1]['bitbucket'] = 'https://bitbucket...';
//$alunos[2]['nome'] = 'Fulano2';
//$alunos[2]['bitbucket'] = 'https://bitbucket...';
//$alunos[3]['nome'] = 'Fulano3';
//$alunos[3]['bitbucket'] = 'https://bitbucket...';
//$alunos[4]['nome'] = 'Fulano3';
//$alunos[4]['bitbucket'] = 'https://bitbucket...';
//$alunos[5]['nome'] = 'Fulano4';
//$alunos[5]['bitbucket'] = 'https://bitbucket...';

//  var_dump($alunos);



//Outra forma

$alunos = array(0 => array('nome' => 'Fulano...',
                       'bitbucket' => 'https://bitbucket' ),
                1 => array('nome' => 'Fulano1...',
                       'bitbucket' => 'https://bitbucket' ),
                2 => array('nome' => 'Fulano2...',
                       'bitbucket' => 'https://bitbucket' ),
                3 => array('nome' => 'Fulano3...',
                       'bitbucket' => 'https://bitbucket' ),
                4 => array('nome' => 'Fulano4...',
                       'bitbucket' => 'https://bitbucket' ),
                5 => array('nome' => 'Fulano5...',
                       'bitbucket' => 'https://bitbucket' ));


var_dump($alunos);

?>