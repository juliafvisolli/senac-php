<?php
echo "<pre>";

    //Constante no PHP
define('QNT_PAGINAS', 10);

echo "Valor da minha constante é:" . QNT_PAGINAS;

//variável para passar valor para uma constante
//$qnt_paginas = 25;

$ip_do_banco = '192.168.45.12';

define('IP_DO_BANCO', $ip_do_banco);

echo "\nO IP do SGBD é" . IP_DO_BANCO;


//Constantes mágicas
echo "\nEstou na linha: " . __LINE__;
echo "\nEstou na linha: " . __LINE__;
echo "\nEste é o arquivo: " . __FILE__;

echo "<pre>";



//Muito bom para decorar o codigo
echo "\n\n";
var_dump($ip_do_banco);

/*Agora fica mais legal
*é hora do vetor!
*Array
*/

$dias_da_semana = ['Dom',
                   'Seg',
                   'Ter',
                   'Qua',
                   'Qui',
                   'Sex',
                   'Sab'];

unset($dias_da_semana); //destroi a variável

/*$dias_da_semana [0] = 'Dom'
$dias_da_semana [1] = 'Seg'
$dias_da_semana [2] = 'Ter'
$dias_da_semana [3] = 'Qua'
$dias_da_semana [4] = 'Qui'
$dias_da_semana [5] = 'Sex'
$dias_da_semana [6] = 'Sab'*/


$dias_da_semana = array(0 => 'Dom', 
                        1 => 'Seg', 
                        3 => 'Ter', 
                        4 => 'Qua', 
                        5 => 'Qui', 
                        6 => 'Sex', 
                        7 => 'Sab');

echo "\n\n";

var_dump($dias_da_semana);


    







?>